// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use Error;
use GLAPI;
use GLContext;
use GLDisplayType;
use GLWindow;
use ffi;
use glib;
use glib::object::Downcast;
use glib::object::IsA;
use glib::signal::SignalHandlerId;
use glib::signal::connect_raw;
use glib::translate::*;
use glib_ffi;
use gst;
use gst_ffi;
use std::boxed::Box as Box_;
use std::mem::transmute;
use std::ptr;

glib_wrapper! {
    pub struct GLDisplay(Object<ffi::GstGLDisplay, ffi::GstGLDisplayClass>): [
        gst::Object => gst_ffi::GstObject,
    ];

    match fn {
        get_type => || ffi::gst_gl_display_get_type(),
    }
}

impl GLDisplay {
    pub fn new() -> GLDisplay {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_full(ffi::gst_gl_display_new())
        }
    }
}

impl Default for GLDisplay {
    fn default() -> Self {
        Self::new()
    }
}

unsafe impl Send for GLDisplay {}
unsafe impl Sync for GLDisplay {}

pub trait GLDisplayExt: 'static {
    fn add_context(&self, context: &GLContext) -> Result<(), glib::error::BoolError>;

    fn create_context(&self, other_context: &GLContext) -> Result<GLContext, Error>;

    fn create_window(&self) -> Option<GLWindow>;

    fn filter_gl_api(&self, gl_api: GLAPI);

    fn get_gl_api(&self) -> GLAPI;

    fn get_gl_api_unlocked(&self) -> GLAPI;

    fn get_handle_type(&self) -> GLDisplayType;

    fn remove_window(&self, window: &GLWindow) -> Result<(), glib::error::BoolError>;

    fn connect_create_context<F: Fn(&Self, &GLContext) -> GLContext + Send + Sync + 'static>(&self, f: F) -> SignalHandlerId;
}

impl<O: IsA<GLDisplay>> GLDisplayExt for O {
    fn add_context(&self, context: &GLContext) -> Result<(), glib::error::BoolError> {
        unsafe {
            glib::error::BoolError::from_glib(ffi::gst_gl_display_add_context(self.to_glib_none().0, context.to_glib_none().0), "Failed to add OpenGL context")
        }
    }

    fn create_context(&self, other_context: &GLContext) -> Result<GLContext, Error> {
        unsafe {
            let mut p_context = ptr::null_mut();
            let mut error = ptr::null_mut();
            let _ = ffi::gst_gl_display_create_context(self.to_glib_none().0, other_context.to_glib_none().0, &mut p_context, &mut error);
            if error.is_null() { Ok(from_glib_full(p_context)) } else { Err(from_glib_full(error)) }
        }
    }

    fn create_window(&self) -> Option<GLWindow> {
        unsafe {
            from_glib_full(ffi::gst_gl_display_create_window(self.to_glib_none().0))
        }
    }

    fn filter_gl_api(&self, gl_api: GLAPI) {
        unsafe {
            ffi::gst_gl_display_filter_gl_api(self.to_glib_none().0, gl_api.to_glib());
        }
    }

    fn get_gl_api(&self) -> GLAPI {
        unsafe {
            from_glib(ffi::gst_gl_display_get_gl_api(self.to_glib_none().0))
        }
    }

    fn get_gl_api_unlocked(&self) -> GLAPI {
        unsafe {
            from_glib(ffi::gst_gl_display_get_gl_api_unlocked(self.to_glib_none().0))
        }
    }

    fn get_handle_type(&self) -> GLDisplayType {
        unsafe {
            from_glib(ffi::gst_gl_display_get_handle_type(self.to_glib_none().0))
        }
    }

    fn remove_window(&self, window: &GLWindow) -> Result<(), glib::error::BoolError> {
        unsafe {
            glib::error::BoolError::from_glib(ffi::gst_gl_display_remove_window(self.to_glib_none().0, window.to_glib_none().0), "Failed to remove window")
        }
    }

    fn connect_create_context<F: Fn(&Self, &GLContext) -> GLContext + Send + Sync + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<Box_<Fn(&Self, &GLContext) -> GLContext + Send + Sync + 'static>> = Box_::new(Box_::new(f));
            connect_raw(self.to_glib_none().0 as *mut _, b"create-context\0".as_ptr() as *const _,
                transmute(create_context_trampoline::<Self> as usize), Box_::into_raw(f) as *mut _)
        }
    }
}

unsafe extern "C" fn create_context_trampoline<P>(this: *mut ffi::GstGLDisplay, context: *mut ffi::GstGLContext, f: glib_ffi::gpointer) -> *mut ffi::GstGLContext
where P: IsA<GLDisplay> {
    let f: &&(Fn(&P, &GLContext) -> GLContext + Send + Sync + 'static) = transmute(f);
    f(&GLDisplay::from_glib_borrow(this).downcast_unchecked(), &from_glib_borrow(context)).to_glib_full()
}
